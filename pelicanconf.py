#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'iturde'
SITENAME = u'Hache Blog'
SITEURL = ''

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = u'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True

THEME = "themes/notmyidea"

STATIC_COMMENTS = True
#STATIC_COMMENTS_DIR = 'comments' # default
PLUGIN_PATH = 'plugins'
PLUGINS = ["static_comments"]
