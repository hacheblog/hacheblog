Title: Pelican plugin for static comments
Slug: pelican-static-comments
Date: 2014-03-04
Tags: pelican

I wanted to add comments to this blog but I didn't want to use an external
service for them. So I went back to the old and proven "email me your comments!"
approach. I dont't see that specially intrincate for the commenter, considering
that most comment systems already request an email address _and_ ask you to
solve a capcha.

To publish the comments in the blog I have written
[static_comments](https://github.com/getpelican/pelican-plugins/tree/master/static_comments),
a small plugin for [pelican](http://getpelican.com/), the static site generator used
to build this blog.

Basically for each article there is a file with the comments. The plugin looks
for the comments file and adds them to the blog entry.

Details about how to configure the blog to use the plugin can be found in the
[blog repo](https://gitorious.org/hacheblog).

